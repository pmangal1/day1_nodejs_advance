// 'Pass BY Value or Reference Starts'

function square(side) {
	let area = side * side;
	return area;
}
var side = 10;
var result = square(side);
console.log(side); // 10 -- no change
console.log(result); // 100 

function turnOn(machine) {
	machine.isOn = true;
}

var computer = {
	isOn: false
};

turnOn(computer);
console.log(computer.isOn); // true;
