// module pattern starts
var employeeDetails = function () {
	// private
	let name = "prakhar";
	let age = 21;
	let sal = 10000;

	function showEmployeeInfo() {
		console.log(`${name} is ${age} years old with salary ${sal}`)
	}

	// public
	return {
		name,
		showEmployeeInfo
	}
}

prakhar = employeeDetails()
console.log(prakhar.name) // prakhar
console.log(prakhar.age) // undefined
prakhar.showEmployeeInfo() // prakhar is 21 years old with salary 10000
