const person = {
	firstName: 'John',
	lastName: 'Doe'
};

/* 
using spread ... 
Deep For plain object
Shallow for nested object
*/
let personOne = {
	...person
};

/*
using  Object.assign() method 
Deep for plain object
Shallow for nested object
*/
let personTwo = Object.assign({}, person);

// using JSON (Deep Copy)
let personThree = JSON.parse(JSON.stringify(person));
