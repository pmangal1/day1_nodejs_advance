// apply call bind starts
foodItem = {
	item: 'tea',
	price: 10,
}

foodItem2 = {
	item: 'coffee',
	price: 20
}

function total(cafe) {
	console.log(`${this.item} RS ${this.price} Cafe ${cafe}`);
}

// call
total.call(foodItem, 'Tinkus') // tea RS 10 Cafe Tinkus

// apply
total.apply(foodItem2, ['Karwaan']) // coffee RS 20 Cafe Karwaan 

// bind
foodItem3 = {
	item: 'shake',
	price: 100
}

let showTotal = total.bind(foodItem3)
showTotal('CCD') // shake RS 100 Cafe CCD
