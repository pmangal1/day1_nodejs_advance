// new keyword starts
function Person(name, age) {
  this.name = name;
  this.age = age;
}

personOne = new Person('Prakhar', 21);
personTwo = new Person('Mangal', 20);

function CarDetails(make, model, year, owner) {
  this.make = make;
  this.model = model;
  this.year = year;
  this.owner = owner;
}

var carOne = new CarDetails('Eagle', 'Talon TSi', 1993, personOne);
var carTwo = new CarDetails('Nissan', '300ZX', 1992, personTwo);

console.log(`CAR: ${carOne.make} | OWNER: ${carOne.owner.name}`); // CAR: Eagal | OWNER: Prakhar
console.log(`CAR: ${carTwo.make} | OWNER: ${carTwo.owner.name}`); // CAR: Nissan | OWNER: Mangal
