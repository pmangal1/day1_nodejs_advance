// Spread Operators
function sum(x, y, z) {
	console.log(x + y + z)
}

numbers = [1, 2, 3]

sum(...numbers) // spreading array

a = [1, 2, 3]
b = [4, 5, 6]

c = [...a, ...b] // [1,2,3,4,5,6]
