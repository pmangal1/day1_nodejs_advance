// closure starts

function counter() {
	let count = 0;

	return function () {
		count++;
		console.log(count)
	}
}

countOne = counter()

countOne() // 1
countOne() // 2

countTwo = counter()

countTwo() // 1
countTwo() // 2
countTwo() // 3
