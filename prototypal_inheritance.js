/* 
prototypal inheritance starts
Inheriting properties
*/
let polygon = function () {
	this.sideOne = 1;
	this.sideTwo = 2;
};

let triangle = new polygon();

polygon.prototype.sideTwo = 3;
polygon.prototype.sideThree = 4;

console.log(triangle.sideOne) // 1
console.log(triangle.sideTwo) // 2
console.log(triangle.sideThree) // 4
console.log(triangle.sideFour) // undefined


// inheriting methods
var sample = {
	a: 2,
	m: function () {
		return this.a + 1;
	}
};

console.log(sample.m()); // 3

var sampleOne = Object.create(sample)
sampleOne.a = 4
console.log(sampleOne.m()) // 5

// With Constructor
function Graph() {
	this.vertices = [];
	this.edges = [];
}

Graph.prototype = {
	addVertex: function (v) {
		this.vertices.push(v);
	},
	addEdges: function (e) {
		this.edges.push(e)
	}
}

var g = new Graph();
g.addVertex(4)
g.addVertex(5)
console.log(g.vertices)
